FROM registry.gitlab.com/dreamer-labs/repoman/dl-kcov-test/image:latest

ARG INSTALL_DIR=/opt

COPY --from=registry.gitlab.com/dreamer-labs/repoman/dl-shunit2-test/image:latest ${INSTALL_DIR}/shunit2 ${INSTALL_DIR}/shunit2

RUN ln -s /opt/shunit2/shunit2 /usr/local/bin/shunit2 && \
    yum install -y git which

CMD ["/bin/bash"]
