# 1.0.0 (2019-12-03)


### Bug Fixes

* add shunit2 to path ([e9639ec](https://gitlab.com/dreamer-labs/repoman/dl-bash-test/commit/e9639ec))
